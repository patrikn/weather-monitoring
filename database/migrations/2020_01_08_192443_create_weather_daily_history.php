<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWeatherDailyHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weather_daily_history', function (Blueprint $table) {
            $table->integer('city_id');
            $table->date("date");
            $table->double("temperature_average", 5, 2);
            $table->double("temperature_max", 5, 2);
            $table->double("temperature_min", 5, 2);
            $table->integer("pressure_average");
            $table->double("humidity_average", 3, 2);
            $table->double("wind_speed_average", 4, 1);
            $table->double("wind_speed_max", 4, 1);
            $table->integer("wind_direction_average")->nullable();
            $table->double("wind_gust_max", 4, 1)->nullable();
            $table->double("cloudiness_average", 3, 2);
            $table->double("cloudiness_min", 3, 2);
            $table->double("cloudiness_max", 3, 2);
            $table->double("rain", 5, 2)->nullable();
            $table->double("snow", 5, 2)->nullable();

            $table->primary(["city_id", "date"]);
            $table->foreign("city_id")->references("openweathermap_city_id")->on("city");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weather_daily_history');
    }
}
