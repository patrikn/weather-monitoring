<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateBasicStructure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->createContinentTable();
        $this->createCountryTable();
        $this->createCountryContinentRelationshipTable();
        $this->createCityTable();
        $this->createOpenWeatherMapWeatherTable();
        $this->createCurrentConditionsTable();
        $this->createCurrentConditionsWeatherTable();
    }

    private function createContinentTable() {
        Schema::create("continent", function (Blueprint $table) {
            $table->string("code", 2)->primary();
            $table->string("name", 16);
        });
    }

    private function createCountryTable() {
        Schema::create("country", function (Blueprint $table) {
            $table->string("code", 3)->primary();
            $table->string("name", 48);
        });
    }

    private function createCountryContinentRelationshipTable() {
        Schema::create("country_continent", function (Blueprint $table) {
            $table->increments("id");
            $table->string("country_code", 3);
            $table->string("continent_code", 2);

            $table->unique(["country_code", "continent_code"]);
            $table->foreign("country_code")->references("code")->on("country");
            $table->foreign("continent_code")->references("code")->on("continent");
        });
    }

    private function createCityTable() {
        Schema::create("city", function (Blueprint $table) {
            $table->integer("openweathermap_city_id")->primary();
            $table->string("name", 64);
            $table->string("country_code", 3);
            $table->boolean("is_capital")->default(false);
            $table->decimal("lat", 9, 6);
            $table->decimal("lon", 9, 6);
            $table->integer("population");
            $table->boolean("is_active")->default(true);

            $table->foreign("country_code")->references("code")->on("country");
        });
    }

    private function createOpenWeatherMapWeatherTable() {
        Schema::create("openweathermap_weather", function (Blueprint $table) {
            $table->integer("openweathermap_weather_id")->primary();
            $table->string("description", 64);
        });
    }

    private function createCurrentConditionsTable()
    {
        Schema::create("current_conditions", function (Blueprint $table) {
            $table->bigIncrements("current_conditions_id");
            $table->integer("city_id");
            $table->double("temperature", 5, 2);
            $table->double("feels_like_temperature", 5, 2);
            $table->integer("pressure");
            $table->double("humidity", 3, 2);
            $table->double("wind_speed", 4, 1);
            $table->integer("wind_direction")->nullable();
            $table->double("wind_gust", 4, 1)->nullable();
            $table->double("cloudiness", 3, 2);
            $table->double("rain_1h", 5, 2)->nullable();
            $table->double("rain_3h", 5, 2)->nullable();
            $table->double("snow_1h", 5, 2)->nullable();
            $table->double("snow_3h", 5, 2)->nullable();
            $table->timestamp("openweathermap_update_time")->useCurrent()->index();
            $table->timestamp("update_time")->useCurrent()->index();

            $table->foreign("city_id")->references("openweathermap_city_id")->on("city");
        });
    }

    private function createCurrentConditionsWeatherTable() {
        Schema::create("current_conditions_weather", function (Blueprint $table) {
            $table->increments("id");
            $table->unsignedBigInteger("current_conditions_id");
            $table->integer("openweathermap_weather_id");

            $table->unique(["current_conditions_id", "openweathermap_weather_id"], "current_conditions_id_openweathermap_weather_id_unique");
            $table->foreign("current_conditions_id")->references("current_conditions_id")->on("current_conditions")->onDelete("cascade");
            $table->foreign("openweathermap_weather_id")->references("openweathermap_weather_id")->on("openweathermap_weather");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('current_conditions_weather');
        Schema::dropIfExists('current_conditions');
        Schema::dropIfExists('openweathermap_weather');
        Schema::dropIfExists('current_conditions');
        Schema::dropIfExists('city');
        Schema::dropIfExists('country_continent');
        Schema::dropIfExists('country');
        Schema::dropIfExists('continent');
    }
}
