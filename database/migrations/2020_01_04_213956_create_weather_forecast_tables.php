<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWeatherForecastTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weather_forecast', function (Blueprint $table)
        {
            $table->bigIncrements("id");
            $table->integer("city_id");
            $table->timestamp("forecast_time");
            $table->double("temperature", 5, 2);
            $table->integer("pressure");
            $table->double("humidity", 3, 2);
            $table->double("wind_speed", 4, 1);
            $table->integer("wind_direction")->nullable();
            $table->double("cloudiness", 3, 2);
            $table->double("rain", 5, 2)->nullable();
            $table->double("snow", 5, 2)->nullable();
            $table->timestamp("update_time")->useCurrent()->index();

            $table->foreign("city_id")->references("openweathermap_city_id")->on("city");
            $table->unique(["city_id", "forecast_time"]);
        });

        Schema::create("weather_forecast_weather", function (Blueprint $table) {
            $table->increments("id");
            $table->unsignedBigInteger("weather_forecast_id");
            $table->integer("openweathermap_weather_id");

            $table->unique(["weather_forecast_id", "openweathermap_weather_id"], "weather_forecast_id_openweathermap_weather_id_unique");
            $table->foreign("weather_forecast_id")->references("id")->on("weather_forecast")->onDelete("cascade");
            $table->foreign("openweathermap_weather_id")->references("openweathermap_weather_id")->on("openweathermap_weather");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weather_forecast_weather');
        Schema::dropIfExists('weather_forecast');
    }
}
