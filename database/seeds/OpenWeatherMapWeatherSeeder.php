<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OpenWeatherMapWeatherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = [
            [200, 'thunderstorm with light rain', 'búrka doprevádzaná ľahkým dažďom'],
            [201, 'thunderstorm with rain', 'búrka doprevádzaná dažďom'],
            [202, 'thunderstorm with heavy rain', 'búrka doprevádzaná silným dažďom'],
            [210, 'light thunderstorm', 'slabá búrka'],
            [211, 'thunderstorm', 'búrka'],
            [212, 'heavy thunderstorm', 'silná búrka'],
            [221, 'ragged thunderstorm', 'prudká búrka'],
            [230, 'thunderstorm with light drizzle', 'búrka doprevádzaná slabým mrholením'],
            [231, 'thunderstorm with drizzle', 'búrka doprevádzaná mrholením'],
            [232, 'thunderstorm with heavy drizzle', 'búrka doprevádzaná silnejším mrholením'],
            [300, 'light intensity drizzle', 'slabé mrholenie'],
            [301, 'drizzle', 'mrholenie'],
            [302, 'heavy intensity drizzle', 'silnejšie mrholenie'],
            [310, 'light intensity drizzle rain', 'slabé mrholenie'],
            [311, 'drizzle rain', 'silnejšie mrholenie'],
            [312, 'heavy intensity drizzle rain', 'silnejšie mrholenie'],
            [313, 'shower rain and drizzle', 'silnejšie mrholenie'],
            [314, 'heavy shower rain and drizzle', 'silnejšie mrholenie'],
            [321, 'shower drizzle', 'silnejšie mrholenie'],
            [500, 'light rain', 'slabý dážď'],
            [501, 'moderate rain', 'dážď'],
            [502, 'heavy intensity rain', 'dážď'],
            [503, 'very heavy rain', 'silný dážď'],
            [504, 'extreme rain', 'dilný dážď'],
            [511, 'freezing rain', 'mrznúci dážď'],
            [520, 'light intensity shower rain', 'slabý dážď'],
            [521, 'shower rain', 'dážď'],
            [522, 'heavy intensity shower rain', 'silný dážď'],
            [531, 'ragged shower rain', 'silný dážď'],
            [600, 'light snow', 'slabé sneženie'],
            [601, 'snow', 'sneženie'],
            [602, 'heavy snow', 'silné sneženie'],
            [611, 'sleet', 'dážď so snehom'],
            [612, 'shower sleet', 'dážď so snehom'],
            [615, 'light rain and snow', 'slabý dážď so snehom'],
            [616, 'rain and snow', 'dážď so snehom'],
            [620, 'light shower snow', 'slabé sneženie'],
            [621, 'shower snow', 'sneženie'],
            [622, 'heavy shower snow', 'silné sneženie'],
            [701, 'mist', 'hmla'],
            [711, 'smoke', 'dym'],
            [721, 'haze', 'opar'],
            [731, 'sand, dust whirls', 'pieskový vír'],
            [741, 'fog', 'hmla'],
            [751, 'sand', 'piesok'],
            [761, 'dust', 'prach'],
            [762, 'volcanic ash', 'sopečný popol'],
            [771, 'squalls', 'nárazy vetra'],
            [781, 'tornado', 'tornádo'],
            [800, 'clear sky', 'jasno'],
            [801, 'few clouds', 'polojasno'],
            [802, 'scattered clouds', 'polooblačno'],
            [803, 'broken clouds', 'polooblačno'],
            [804, 'overcast clouds', 'oblačno'],
            [900, 'tornado', 'tornádo'],
            [901, 'tropical storm', 'tropická búrka'],
            [902, 'hurricane', 'hurikán'],
            [903, 'cold', 'chladno'],
            [904, 'hot', 'horúco'],
            [905, 'windy', 'veterno'],
            [906, 'hail', 'krupobitie'],
            [951, 'calm', 'bezvetrie'],
            [952, 'light breeze', 'ľahký vánok'],
            [953, 'gentle breeze', 'jemný vánok'],
            [954, 'moderate breeze', 'mierny vietor'],
            [955, 'fresh breeze', 'čerstvý vietor'],
            [956, 'strong breeze', 'silný vietor'],
            [957, 'high wind, near gale', 'silný vietor'],
            [958, 'gale', 'víchrica'],
            [959, 'severe gale', 'silná víchrica'],
            [960, 'storm', 'búrka'],
            [961, 'violent storm', 'prudká búrka'],
            [962, 'hurricane', 'hurikán']
        ];
        foreach ($arr as $item) {
            DB::table("openweathermap_weather")->insert([
                "openweathermap_weather_id" => $item[0],
                "description" => $item[1]
            ]);
        }
    }

}
