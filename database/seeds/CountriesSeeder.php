<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertContinents();
        $this->insertCountries();
    }

    private function insertContinents()
    {
        DB::table("continent")->insert(["code" => "EU", "name" => "Európa"]);
        DB::table("continent")->insert(["code" => "AS", "name" => "Ázia"]);
        DB::table("continent")->insert(["code" => "NA", "name" => "Severná Amerika"]);
        DB::table("continent")->insert(["code" => "SA", "name" => "Južná Amerika"]);
        DB::table("continent")->insert(["code" => "AF", "name" => "Afrika"]);
        DB::table("continent")->insert(["code" => "AU", "name" => "Austrália"]);
        DB::table("continent")->insert(["code" => "AN", "name" => "Antarktída"]);
    }

    private function insertCountries()
    {
        $countries = array_merge(
            $this->getEuropeanCountries(),
            $this->getAsianCountries(),
            $this->getAfricanCountries(),
            $this->getNorthAmericanCountries(),
            $this->getSouthAmericanCountries(),
            $this->getAustralianCountries()
        );

        foreach ($countries as $country) {
            DB::table("country")->insert(["code" => $country["code"], "name" => $country["name"]]);
            $continents = $country["continents"];
            foreach ($continents as $continent) {
                DB::table("country_continent")->insert(["country_code" => $country["code"], "continent_code" => $continent]);
            }
        }
    }

    private function getEuropeanCountries()
    {
        return [
            ["code" => "ALB", "name" => "Albánsko", "continents" => ["EU"]],
            ["code" => "AND", "name" => "Andorra", "continents" => ["EU"]],
            ["code" => "AUT", "name" => "Rakúsko", "continents" => ["EU"]],
            ["code" => "BLR", "name" => "Bielorusko", "continents" => ["EU"]],
            ["code" => "BEL", "name" => "Belgicko", "continents" => ["EU"]],
            ["code" => "BIH", "name" => "Bosna a Hercegovina", "continents" => ["EU"]],
            ["code" => "BUL", "name" => "Bulharsko", "continents" => ["EU"]],
            ["code" => "CRO", "name" => "Chorvátsko", "continents" => ["EU"]],
            ["code" => "CZE", "name" => "Česko", "continents" => ["EU"]],
            ["code" => "DEN", "name" => "Dánsko", "continents" => ["EU"]],
            ["code" => "EST", "name" => "Estónsko", "continents" => ["EU"]],
            ["code" => "FIN", "name" => "Fínsko", "continents" => ["EU"]],
            ["code" => "FRA", "name" => "Francúzsko", "continents" => ["EU"]],
            ["code" => "GER", "name" => "Nemecko", "continents" => ["EU"]],
            ["code" => "GRE", "name" => "Grécko", "continents" => ["EU"]],
            ["code" => "HUN", "name" => "Maďarsko", "continents" => ["EU"]],
            ["code" => "ICE", "name" => "Island", "continents" => ["EU"]],
            ["code" => "IRE", "name" => "Írsko", "continents" => ["EU"]],
            ["code" => "ITA", "name" => "Taliansko", "continents" => ["EU"]],
            ["code" => "KOS", "name" => "Kosovo", "continents" => ["EU"]],
            ["code" => "LAT", "name" => "Lotyšsko", "continents" => ["EU"]],
            ["code" => "LIE", "name" => "Lichtenštajnsko", "continents" => ["EU"]],
            ["code" => "LIT", "name" => "Litva", "continents" => ["EU"]],
            ["code" => "LUX", "name" => "Luxembursko", "continents" => ["EU"]],
            ["code" => "NMA", "name" => "Severné Macedónsko", "continents" => ["EU"]],
            ["code" => "MLT", "name" => "Malta", "continents" => ["EU"]],
            ["code" => "MOL", "name" => "Moldavsko", "continents" => ["EU"]],
            ["code" => "MON", "name" => "Monako", "continents" => ["EU"]],
            ["code" => "MTG", "name" => "Čierna Hora", "continents" => ["EU"]],
            ["code" => "NED", "name" => "Holandsko", "continents" => ["EU"]],
            ["code" => "NOR", "name" => "Nórsko", "continents" => ["EU"]],
            ["code" => "POL", "name" => "Poľsko", "continents" => ["EU"]],
            ["code" => "ROM", "name" => "Rumunsko", "continents" => ["EU"]],
            ["code" => "RUS", "name" => "Rusko", "continents" => ["EU", "AS"]],
            ["code" => "SMR", "name" => "San Maríno", "continents" => ["EU"]],
            ["code" => "SER", "name" => "Srbsko", "continents" => ["EU"]],
            ["code" => "SVK", "name" => "Slovensko", "continents" => ["EU"]],
            ["code" => "SLO", "name" => "Slovinsko", "continents" => ["EU"]],
            ["code" => "SPA", "name" => "Španielsko", "continents" => ["EU"]],
            ["code" => "SWE", "name" => "Švédsko", "continents" => ["EU"]],
            ["code" => "SWI", "name" => "Švajčiarsko", "continents" => ["EU"]],
            ["code" => "UKR", "name" => "Ukrajina", "continents" => ["EU"]],
            ["code" => "UKN", "name" => "Spojené Kráľovstvo", "continents" => ["EU"]],
            ["code" => "VAT", "name" => "Vatikán", "continents" => ["EU"]]
        ];
    }

    private function getAsianCountries()
    {
        return [
            ["code" => "AFG", "name" => "Afganistan", "continents" => ["AS"]],
            ["code" => "ARM", "name" => "Arménsko", "continents" => ["AS"]],
            ["code" => "AZE", "name" => "Azerbajdžan", "continents" => ["AS"]],
            ["code" => "BAH", "name" => "Bahrajn", "continents" => ["AS"]],
            ["code" => "BAN", "name" => "Bangladéš", "continents" => ["AS"]],
            ["code" => "BHU", "name" => "Bhután", "continents" => ["AS"]],
            ["code" => "BRU", "name" => "Brunej", "continents" => ["AS"]],
            ["code" => "CBD", "name" => "Kambodža", "continents" => ["AS"]],
            ["code" => "CHI", "name" => "Čína", "continents" => ["AS"]],
            ["code" => "CYP", "name" => "Cyprus", "continents" => ["AS", "EU"]],
            ["code" => "ETI", "name" => "Východný Timor", "continents" => ["AS"]],
            ["code" => "GEO", "name" => "Gruzínsko", "continents" => ["AS"]],
            ["code" => "HKO", "name" => "Hong Kong", "continents" => ["AS"]],
            ["code" => "IND", "name" => "India", "continents" => ["AS"]],
            ["code" => "INN", "name" => "Indonézia", "continents" => ["AS"]],
            ["code" => "IRA", "name" => "Irán", "continents" => ["AS"]],
            ["code" => "IRQ", "name" => "Irak", "continents" => ["AS"]],
            ["code" => "ISR", "name" => "Izrael", "continents" => ["AS"]],
            ["code" => "JAP", "name" => "Japonsko", "continents" => ["AS"]],
            ["code" => "JOR", "name" => "Jordánsko", "continents" => ["AS"]],
            ["code" => "KAZ", "name" => "Kazachstan", "continents" => ["AS"]],
            ["code" => "KUW", "name" => "Kuvajt", "continents" => ["AS"]],
            ["code" => "KYR", "name" => "Kyrgistan", "continents" => ["AS"]],
            ["code" => "LAO", "name" => "Laos", "continents" => ["AS"]],
            ["code" => "LEB", "name" => "Libanon", "continents" => ["AS"]],
            ["code" => "MAL", "name" => "Malajzia", "continents" => ["AS"]],
            ["code" => "MLD", "name" => "Maledivy", "continents" => ["AS"]],
            ["code" => "MNG", "name" => "Mongolsko", "continents" => ["AS"]],
            ["code" => "MYA", "name" => "Mjanmarsko", "continents" => ["AS"]],
            ["code" => "NEP", "name" => "Nepál", "continents" => ["AS"]],
            ["code" => "NKO", "name" => "Severná Kórea", "continents" => ["AS"]],
            ["code" => "OMA", "name" => "Omán", "continents" => ["AS"]],
            ["code" => "PAK", "name" => "Pakistam", "continents" => ["AS"]],
            ["code" => "PAL", "name" => "Palestína", "continents" => ["AS"]],
            ["code" => "PHI", "name" => "Filipíny", "continents" => ["AS"]],
            ["code" => "QAT", "name" => "Katar", "continents" => ["AS"]],
            ["code" => "SAR", "name" => "Saudská Arábia", "continents" => ["AS"]],
            ["code" => "SIN", "name" => "Singapur", "continents" => ["AS"]],
            ["code" => "SKO", "name" => "Južná Kórea", "continents" => ["AS"]],
            ["code" => "SLA", "name" => "Srí Lanka", "continents" => ["AS"]],
            ["code" => "SYR", "name" => "Sýria", "continents" => ["AS"]],
            ["code" => "TAI", "name" => "Tajwan", "continents" => ["AS"]],
            ["code" => "TAJ", "name" => "Tadžikistan", "continents" => ["AS"]],
            ["code" => "THA", "name" => "Thajsko", "continents" => ["AS"]],
            ["code" => "TUR", "name" => "Turecko", "continents" => ["AS", "EU"]],
            ["code" => "TMS", "name" => "Turkmenistan", "continents" => ["AS"]],
            ["code" => "UAE", "name" => "Spojené Arabské Emiráty", "continents" => ["AS"]],
            ["code" => "UZB", "name" => "Uzbekistan", "continents" => ["AS"]],
            ["code" => "VIE", "name" => "Vietnam", "continents" => ["AS"]],
            ["code" => "YEM", "name" => "Jemen", "continents" => ["AS"]]
        ];
    }

    private function getAfricanCountries()
    {
        return [
            ["code" => "ALG", "name" => "Alžírsko", "continents" => ["AF"]],
            ["code" => "ANG", "name" => "Angola", "continents" => ["AF"]],
            ["code" => "BEN", "name" => "Benin", "continents" => ["AF"]],
            ["code" => "BOT", "name" => "Botswana", "continents" => ["AF"]],
            ["code" => "BFA", "name" => "Burkina Faso", "continents" => ["AF"]],
            ["code" => "BUR", "name" => "Burundi", "continents" => ["AF"]],
            ["code" => "CAM", "name" => "Kamerun", "continents" => ["AF"]],
            ["code" => "CVE", "name" => "Kapverdy", "continents" => ["AF"]],
            ["code" => "CAR", "name" => "Stredoafrická Republika", "continents" => ["AF"]],
            ["code" => "CHA", "name" => "Čad", "continents" => ["AF"]],
            ["code" => "COM", "name" => "Komory", "continents" => ["AF"]],
            ["code" => "CON", "name" => "Kongo", "continents" => ["AF"]],
            ["code" => "DRC", "name" => "Demokratická Republika Kongo", "continents" => ["AF"]],
            ["code" => "ICO", "name" => "Pobrežie Slonoviny", "continents" => ["AF"]],
            ["code" => "DJI", "name" => "Džibutsko", "continents" => ["AF"]],
            ["code" => "EGY", "name" => "Egypt", "continents" => ["AF"]],
            ["code" => "EGU", "name" => "Rovníková Guinea", "continents" => ["AF"]],
            ["code" => "ERI", "name" => "Eritrea", "continents" => ["AF"]],
            ["code" => "ETH", "name" => "Etiópia", "continents" => ["AF"]],
            ["code" => "GAB", "name" => "Gabon", "continents" => ["AF"]],
            ["code" => "GAM", "name" => "Gambia", "continents" => ["AF"]],
            ["code" => "GHA", "name" => "Ghana", "continents" => ["AF"]],
            ["code" => "GUI", "name" => "Guinea", "continents" => ["AF"]],
            ["code" => "GBI", "name" => "Guinea-Bissau", "continents" => ["AF"]],
            ["code" => "KEN", "name" => "Keňa", "continents" => ["AF"]],
            ["code" => "LES", "name" => "Lesoto", "continents" => ["AF"]],
            ["code" => "LBE", "name" => "Libéria", "continents" => ["AF"]],
            ["code" => "LIB", "name" => "Líbya", "continents" => ["AF"]],
            ["code" => "MAD", "name" => "Madagaskar", "continents" => ["AF"]],
            ["code" => "MLW", "name" => "Malawi", "continents" => ["AF"]],
            ["code" => "MLI", "name" => "Mali", "continents" => ["AF"]],
            ["code" => "MRT", "name" => "Mauritánia", "continents" => ["AF"]],
            ["code" => "MAU", "name" => "Maurícius", "continents" => ["AF"]],
            ["code" => "MOR", "name" => "Maroko", "continents" => ["AF"]],
            ["code" => "MOZ", "name" => "Mozambik", "continents" => ["AF"]],
            ["code" => "NAM", "name" => "Namíbia", "continents" => ["AF"]],
            ["code" => "NGR", "name" => "Niger", "continents" => ["AF"]],
            ["code" => "NIG", "name" => "Nigéria", "continents" => ["AF"]],
            ["code" => "RWA", "name" => "Rwanda", "continents" => ["AF"]],
            ["code" => "STP", "name" => "Svätý Tomáš a Princov ostrov", "continents" => ["AF"]],
            ["code" => "SEN", "name" => "Senegal", "continents" => ["AF"]],
            ["code" => "SEY", "name" => "Seychely", "continents" => ["AF"]],
            ["code" => "SLE", "name" => "Sierra Leone", "continents" => ["AF"]],
            ["code" => "SOM", "name" => "Somálsko", "continents" => ["AF"]],
            ["code" => "SAF", "name" => "Južná Afrika", "continents" => ["AF"]],
            ["code" => "SSU", "name" => "Južný Sudán", "continents" => ["AF"]],
            ["code" => "SUD", "name" => "Sudán", "continents" => ["AF"]],
            ["code" => "SWA", "name" => "Svazijsko", "continents" => ["AF"]],
            ["code" => "TAN", "name" => "Tanzánia", "continents" => ["AF"]],
            ["code" => "TOG", "name" => "Togo", "continents" => ["AF"]],
            ["code" => "TUN", "name" => "Tunisko", "continents" => ["AF"]],
            ["code" => "UGA", "name" => "Uganda", "continents" => ["AF"]],
            ["code" => "WSA", "name" => "Západná Sahara", "continents" => ["AF"]],
            ["code" => "ZAM", "name" => "Zambia", "continents" => ["AF"]],
            ["code" => "ZIM", "name" => "Zimbabwe", "continents" => ["AF"]],
        ];
    }

    private function getNorthAmericanCountries() {
        return [];
    }

    private function getSouthAmericanCountries()
    {
        return [
            ["code" => "ARG", "name" => "Argentína", "continents" => ["SA"]],
            ["code" => "BOL", "name" => "Bolívia", "continents" => ["SA"]],
            ["code" => "BRA", "name" => "Brazília", "continents" => ["SA"]],
            ["code" => "CHL", "name" => "Čile", "continents" => ["SA"]],
            ["code" => "COL", "name" => "Kolumbia", "continents" => ["SA"]],
            ["code" => "ECU", "name" => "Ekvádor", "continents" => ["SA"]],
            ["code" => "FAL", "name" => "Falkladské ostrovy", "continents" => ["SA"]],
            ["code" => "FGU", "name" => "Francúzska Guiana", "continents" => ["SA"]],
            ["code" => "GUY", "name" => "Guyana", "continents" => ["SA"]],
            ["code" => "PAR", "name" => "Paraguaj", "continents" => ["SA"]],
            ["code" => "PER", "name" => "Peru", "continents" => ["SA"]],
            ["code" => "SGE", "name" => "Južná Georgia a Južné Sandwichove ostrovy", "continents" => ["SA"]],
            ["code" => "SUR", "name" => "Surinam", "continents" => ["SA"]],
            ["code" => "URU", "name" => "Uruguaj", "continents" => ["SA"]],
            ["code" => "VEN", "name" => "Venezuela", "continents" => ["SA"]],
        ];
    }

    private function getAustralianCountries()
    {
        return [
            ["code" => "AUS", "name" => "Austrália", "continents" => ["AU"]],
            ["code" => "FSM", "name" => "Federatívne štáty Mikronézie", "continents" => ["AU"]],
            ["code" => "FIJ", "name" => "Fidži", "continents" => ["AU"]],
            ["code" => "KIR", "name" => "Kiribati", "continents" => ["AU"]],
            ["code" => "MAI", "name" => "Maršalove ostrovy", "continents" => ["AU"]],
            ["code" => "NAU", "name" => "Nauru", "continents" => ["AU"]],
            ["code" => "NZE", "name" => "Nový Zélend", "continents" => ["AU"]],
            ["code" => "PAU", "name" => "Palau", "continents" => ["AU"]],
            ["code" => "PNG", "name" => "Papua - Nová Guinera", "continents" => ["AU"]],
            ["code" => "SAM", "name" => "Samoa", "continents" => ["AU"]],
            ["code" => "SOI", "name" => "Solomon", "continents" => ["AU"]],
            ["code" => "TON", "name" => "Tonga", "continents" => ["AU"]],
            ["code" => "TUV", "name" => "Tuvalu", "continents" => ["AU"]],
            ["code" => "VAN", "name" => "Vanuatu", "continents" => ["AU"]]
        ];
    }

}
