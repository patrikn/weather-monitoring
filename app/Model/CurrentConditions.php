<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CurrentConditions extends Model
{
    protected $table = 'current_conditions';

    protected $primaryKey = 'current_conditions_id';

    public $incrementing = true;

    public $timestamps = false;

    protected $dates = ['update_time', 'openweathermap_update_time'];

    public function weathers()
    {
        return $this->belongsToMany(
            'App\Model\OpenweathermapWeatherCode',
            'current_conditions_weather',
            'current_conditions_id',
            'openweathermap_weather_id'
        );
    }

    public function city()
    {
        return $this->belongsTo('App\Model\City', 'city_id');
    }
}
