<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'city';

    protected $primaryKey = 'openweathermap_city_id';

    public $incrementing = false;

    public $timestamps = false;

    public function country()
    {
        return $this->belongsTo('App\Model\Country');
    }

    public function currentConditions() {
        return $this->hasMany('App\Model\CurrentConditions', 'city_id');
    }

    public function weatherForecasts() {
        return $this->hasMany('App\Model\WeatherForecast', 'city_id');
    }
}
