<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'country';

    protected $primaryKey = 'code';

    protected $keyType = 'string';

    public $incrementing = false;

    public $timestamps = false;

    public function continents()
    {
        return $this->belongsToMany('App\Model\Continent', 'country_continent', 'country_code', 'continent_code');
    }

    public function cities()
    {
        return $this->hasMany('App\Model\City');
    }
}
