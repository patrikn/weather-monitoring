<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Continent extends Model
{

    protected $table = 'continent';

    protected $primaryKey = 'code';

    protected $keyType = 'string';

    public $incrementing = false;

    public $timestamps = false;

    public function countries()
    {
        return $this->belongsToMany('App\Model\Country', 'country_continent', 'continent_code', 'country_code');
    }

}
