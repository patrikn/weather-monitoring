<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OpenweathermapWeatherCode extends Model
{
    protected $table = 'openweathermap_weather';

    protected $primaryKey = 'openweathermap_weather_id';

    public $incrementing = false;

    public $timestamps = false;

    public function currentConditions()
    {
        return $this->belongsToMany(
            'App\Model\CurrentConditions',
            'current_conditions_weather',
            'openweathermap_weather_id',
            'current_conditions_id'
        );
    }

    public function weatherForecasts()
    {
        return $this->belongsToMany(
            'App\Model\WeatherForecast',
            'weather_forecast_weather',
            'openweathermap_weather_id',
            'weather_forecast_id'
        );
    }
}
