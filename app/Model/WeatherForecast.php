<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class WeatherForecast extends Model
{
    protected $table = 'weather_forecast';

    protected $primaryKey = 'id';

    public $incrementing = true;

    public $timestamps = false;

    protected $dates = ['forecast_time'];

    public function weathers()
    {
        return $this->belongsToMany(
            'App\Model\OpenweathermapWeatherCode',
            'weather_forecast_weather',
            'weather_forecast_id',
            'openweathermap_weather_id'
        );
    }

    public function city()
    {
        return $this->belongsTo('App\Model\City', 'city_id');
    }
}
