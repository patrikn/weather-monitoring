<?php
/**
 * Created by PhpStorm.
 * User: patri
 * Date: 29. 12. 2019
 * Time: 0:30
 */

namespace App\Http\Controllers;


use App\Model\City;
use App\Model\Continent;
use App\Model\Country;
use Illuminate\Http\Request;

class InternalApiController extends Controller
{

    public function getCities()
    {
        $cities = City::with(["country" => function ($query) {
            return $query->with("continents");
        }])->get()->all();

        return response()->json($cities);
    }

    public function getCountriesByContinentCode(Request $request, string $continentCode)
    {
        if ($continentCode === "all") {
            $result = Continent::with(["countries" => function ($query) {
                $query->orderBy("name");
            }])->get()->all();
        } else {
            $result = Continent::where("code", $continentCode)->with(["countries" => function ($query) {
                $query->orderBy("name");
            }])->first();
        }

        return response()->json($result);
    }

    public function getContinents(Request $request)
    {
        $continents = Continent::all()->all();

        if ($request->exists("getOptionsForAdminCities")) {
            $continents = array_map(function($item) {
                return ["id" => $item->code, "text" => $item->name];
            }, $continents);
            array_unshift($continents, ["id" => "all", "text" => "Všetky kontinenty"]) ;
        }

        return response()->json($continents);
    }
}