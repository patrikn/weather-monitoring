<?php
/**
 * Created by PhpStorm.
 * User: patri
 * Date: 2. 1. 2020
 * Time: 14:21
 */

namespace App\Http\Controllers;


use App\Model\City;
use App\Model\CurrentConditions;
use App\Model\OpenweathermapWeatherCode;
use App\Model\WeatherForecast;
use DateTimeZone;
use Illuminate\Support\Facades\DB;

class CityController extends Controller
{

    public function renderCity($cityID)
    {
        $city = $this->getCity($cityID);
        if (!$city) {
            return response("", 404);
        }

        $conditions = $city->currentConditions->all();
        $currentCondition = (count($conditions) ? $conditions[0] : null);
        $conditions = $this->processConditionsData($conditions);
        $forecastData = $this->processForecastData($city->weatherForecasts->all());

        return view("city.city", [
            "city" => $city,
            "conditionsHistory" => $conditions,
            "currentCondition" => $currentCondition,
            "weatherForecast" => $forecastData
        ]);
    }

    private function processForecastData(array $forecastData)
    {
        $dataGroupedByDay = [];
        foreach ($forecastData as $forecast) {
            $day = $forecast->forecast_time->setTimezone("Europe/Bratislava")->format("N");
            $dataGroupedByDay[$day][] = $forecast;
        }
        $now = now();
        if ($now->hour >= 18 && count($dataGroupedByDay) === 6) {
            unset($dataGroupedByDay[$now->format("N") * 1]);
        } else {
            unset($dataGroupedByDay[($now->format("N") - 1 + 5) % 7 + 1]);
        }

        $dayNamesSK = array(
            'pondelok',
            'utorok',
            'streda',
            'štvrtok',
            'piatok',
            'sobota',
            'nedeľa',
        );

        $data = [];
        foreach ($dataGroupedByDay as $dayIndex => $forecasts) {
            $maxTemperature = null;
            $minTemperature = null;
            $pressures = [];
            $maxWindSpeed = null;
            $rain = 0;
            $snow = 0;
            $weatherArray = [];
            $weatherCodesArray = [];
            foreach ($forecasts as $forecast) {
                /** @var WeatherForecast $forecast */
                if ($maxTemperature === null || $forecast->temperature > $maxTemperature) {
                    $maxTemperature = $forecast->temperature;
                }
                if ($minTemperature === null || $forecast->temperature < $minTemperature) {
                    $minTemperature = $forecast->temperature;
                }
                $pressures[] = $forecast->pressure;
                if ($maxWindSpeed === null || $forecast->wind_speed > $maxWindSpeed) {
                    $maxWindSpeed = $forecast->wind_speed;
                }
                $rain += (is_null($forecast->rain) ? 0 : $forecast->rain);
                $snow += (is_null($forecast->snow) ? 0 : $forecast->snow);
                $weathers = $forecast->weathers->all();
                foreach ($weathers as $weather) {
                    /** @var OpenweathermapWeatherCode $weather */
                    $weatherCodesArray[] = $weather->openweathermap_weather_id;
                    $weatherArray[] = $weather;
                }
            }
            $averagePressure = array_sum($pressures) * 1.0 / count($pressures);
            $weatherCodesArray = array_count_values($weatherCodesArray);
            $averageWeatherCode = array_search(max($weatherCodesArray), $weatherCodesArray);
            $averageWeather = null;
            foreach ($weatherArray as $weather) {
                if ($weather->openweathermap_weather_id == $averageWeatherCode) {
                    $averageWeather = $weather;
                    break;
                }
            }
            $data[$dayNamesSK[$dayIndex-1]] = [
                "maxTemperature" => number_format(ceil($maxTemperature),0),
                "minTemperature" => number_format(floor($minTemperature),0),
                "averagePressure" => round($averagePressure),
                "maxWindSpeed" => ceil($maxWindSpeed),
                "rain" => round($rain),
                "snow" => round($snow),
                "averageWeather" => $averageWeather
            ];
        }

        return $data;
    }

    private function processConditionsData(array $conditionsArray) {
        $data = [];
        foreach ($conditionsArray as $conditions) {
            $data[] = [
                "date" => $conditions->openweathermap_update_time->format("U"),
                "temperature" => $conditions->temperature,
                "feels_like_temp" => $conditions->feels_like_temperature,
                "wind_speed" => $conditions->wind_speed
            ];
        }
        return array_reverse($data);
    }

    private function getCity($cityID): ?City
    {
        $dayago = now("UTC")->subDay();
        /** @var City $city */
        $city = City::with([
            "currentConditions" => function($query) use ($dayago) {
                return $query->where("update_time", ">=", $dayago)->orderBy("update_time", "desc");
            },
            "weatherForecasts" => function($query) {
                return $query->with(["weathers"])->orderBy("forecast_time", "asc");
            }
        ])->find($cityID);
        return $city;
    }

}