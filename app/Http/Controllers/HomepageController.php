<?php
/**
 * Created by PhpStorm.
 * User: patri
 * Date: 28. 12. 2019
 * Time: 23:33
 */

namespace App\Http\Controllers;


use App\Model\City;
use App\Model\Country;
use Illuminate\Support\Facades\Storage;

class HomepageController extends Controller
{
    public function render()
    {
        $data = $this->getCities()->all();

        return view("homepage", [
            "countries" => $data
        ]);
    }

    private function getCities() {
        return Country::whereIn("code", ["CZE", "SVK"])->with(["cities" => function($query) {
            return $query->orderBy("population", "desc");
        }])->get();
    }

    private function getData() {
        $currentCityList = json_decode(Storage::disk("public")->get("city.list.json"));

        $currentCityList = array_filter($currentCityList, function ($item) {
            return $item->country == "CZ";
        });

        usort($currentCityList, function ($item1, $item2) {
           return $item2->stat->population - $item1->stat->population;
        });

        $currentCityList = array_map(function ($item) {
            return $item->name . ", " . $item->stat->population;
        }, $currentCityList);

        return [
            "currentCityList" => $currentCityList,
        ];
    }

}