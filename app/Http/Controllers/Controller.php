<?php

namespace App\Http\Controllers;

use App\Model\City;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $cityIDsSK = [3060972, 724443, 3056508, 3061186, 3057124, 723819, 3058531, 3057140];
        $cityIDsEU = [3067696, 2988507, 2950159, 264371, 6539761, 524901, 3117735, 2673730, 2643743, 2761369, 2759794, 3413829];
        $cityIDsWorld = [5128581, 3530597, 3435910, 3451190, 3369157, 360630, 745044, 1850147, 2147714, 292223];
        $cityIDsFavourite = [3060950, 2775220, 2867714, 6542283, 6458783, 2650225, 3176958, 3182164, 2013279, 2013159, 3161733, 293397];

        View::share('countryCities', $this->getCities($cityIDsSK));
        View::share('continentCities', $this->getCities($cityIDsEU));
        View::share('worldCities', $this->getCities($cityIDsWorld));
        View::share('favouriteCities', $this->getCities($cityIDsFavourite));
    }

    private function getCities($cityIDs) {
        $cities = City::with([
            "currentConditions" => function($query) {
                return $query->with("weathers")->orderBy("update_time", "desc")->get()->first();
            }])->whereIn("openweathermap_city_id", $cityIDs)->orderBy("name", "asc")->get()->all();
        return $cities;
    }
}
