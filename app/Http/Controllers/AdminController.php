<?php
/**
 * Created by PhpStorm.
 * User: patri
 * Date: 29. 12. 2019
 * Time: 16:11
 */

namespace App\Http\Controllers;


use App\Model\City;
use App\Model\Continent;

class AdminController extends Controller
{

    public function render()
    {
        return view("admin.admin");
    }

    public function renderCities()
    {
        $continents = Continent::with(["countries" => function ($query) {
            $query->orderBy("name");
        }])->get();

        return view("admin.cities", [
            "continents" => $continents
        ]);
    }

}