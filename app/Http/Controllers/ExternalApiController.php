<?php
/**
 * Created by PhpStorm.
 * User: patri
 * Date: 29. 12. 2019
 * Time: 0:30
 */

namespace App\Http\Controllers;


use App\Model\City;
use App\Model\CurrentConditions;
use App\Model\OpenweathermapWeatherCode;
use App\Model\WeatherForecast;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\DB;

class ExternalApiController
{

    const MINUTE_REQUESTS_COUNT_CURRENT_WEATHER = 50;
    const MINUTE_REQUESTS_COUNT_FORECAST = 10;

    public function extract()
    {
        $apikey = env('OPENWEATHERMAP_API_KEY');

        $cityIDs = array_map(function ($city) {
            return $city->openweathermap_city_id;
        }, $this->getCitiesByOldUpdatedCurrentWeather(self::MINUTE_REQUESTS_COUNT_CURRENT_WEATHER));
        $currentWeatherData = $this->saveCurrentWeatherData($apikey, $cityIDs);

        $cityIDs = array_map(function ($city) {
            return $city->openweathermap_city_id;
        }, $this->getCitiesByOldUpdatedForecast(self::MINUTE_REQUESTS_COUNT_FORECAST));
        $forecastData = $this->saveForecastData($apikey, $cityIDs);

        return response()->json([
            "forecastData" => $forecastData,
            "currentWeatherData" => $currentWeatherData
        ]);
    }

    private function sendRequest($url)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);

        curl_close($ch);
        $data = json_decode($response);

        return $data;
    }

    private function getCitiesByOldUpdatedForecast(int $number) {
        $result = DB::select('
            SELECT C.openweathermap_city_id, C.name, C.population, wf.update_time
            FROM city C LEFT JOIN (
                SELECT city_id, MAX(update_time) as update_time
                FROM weather_forecast
                GROUP BY city_id ) wf ON ( C.openweathermap_city_id = wf.city_id)
            WHERE is_active IS TRUE
            ORDER BY update_time ASC, population DESC
            LIMIT ?
        ', [$number]);
        return $result;
    }

    private function saveForecastData(string $apikey, array $cityIDs)
    {
        $forecastIDs = [];
        foreach ($cityIDs as $cityID) {
            $url = "api.openweathermap.org/data/2.5/forecast?id=" . $cityID . "&appid=" . $apikey . "&units=metric";
            $data = $this->sendRequest($url)->list;

            if ($data) {
                DB::transaction(function () use ($data, &$forecastIDs, $cityID)
                {
                    WeatherForecast::where("city_id", $cityID)->delete();

                    foreach ($data as $forecastData)
                    {
                        $forecast = new WeatherForecast;
                        $forecast->city_id = $cityID;
                        $forecast->forecast_time = DateTime::createFromFormat('U', $forecastData->dt, timezone_open("UTC"));
                        $forecast->temperature = $forecastData->main->temp;
                        $forecast->pressure = $forecastData->main->pressure;
                        $forecast->humidity = $forecastData->main->humidity / 100.0;
                        $forecast->cloudiness = $forecastData->clouds->all / 100.0;
                        $forecast->wind_speed = $forecastData->wind->speed;
                        $forecast->wind_direction = (property_exists($forecastData->wind, "deg") ? $forecastData->wind->deg : null);
                        if (property_exists($forecastData, "rain")) {
                            $rain = $forecastData->rain;
                            $forecast->rain = (property_exists($rain, "3h") ? $rain->{"3h"} : null);
                        }
                        if (property_exists($forecastData, "snow")) {
                            $snow = $forecastData->snow;
                            $forecast->snow = (property_exists($snow, "3h") ? $snow->{"3h"} : null);
                        }
                        $forecast->update_time = Carbon::now("UTC");

                        $weathers = $forecastData->weather;

                        $forecast->save();
                        $forecastIDs[] = $forecast->id;

                        foreach ($weathers as $weather) {
                            DB::table("weather_forecast_weather")->insert([
                                "weather_forecast_id" => $forecast->id,
                                "openweathermap_weather_id" => $weather->id
                            ]);
                        }
                    }
                }, 5);
            }

            if (!($data && count($cityIDs) === count($data))) {
                // getting data for less cities than expected (some cityID is wrong or limit of requests was exceeded)
                // TODO
            }

        }

        $res = WeatherForecast::whereIn("id", $forecastIDs)->with(["weathers", "city"])->get()->all();
        return $res;
    }

    private function getCitiesByOldUpdatedCurrentWeather(int $number) {
        $result = DB::select('
            SELECT C1.openweathermap_city_id, C1.name, C1.population, C2.update_time
            FROM city C1 LEFT JOIN (
                SELECT city_id, MAX(update_time) as update_time
                FROM current_conditions
                GROUP BY city_id ) C2 ON ( C1.openweathermap_city_id = C2.city_id)
            WHERE is_active IS TRUE
            ORDER BY update_time ASC, population DESC
            LIMIT ?
        ', [$number]);
        return $result;
    }

    private function saveCurrentWeatherData(string $apikey, array $cityIDs)
    {
        $chunkSize = 20;
        $cityIDsChunks = array_chunk($cityIDs, $chunkSize);

        $condIDs = [];
        foreach ($cityIDsChunks as $cityIDsChunk)
        {
            $expectedChunkSize = count($cityIDsChunks);

            $cityIDsQuery = implode(",", $cityIDsChunk);
            $url = "http://api.openweathermap.org/data/2.5/group?id=" . $cityIDsQuery . "&appid=" . $apikey . "&units=metric";
            $data = $this->sendRequest($url)->list;

            if ($data) {
                foreach ($data as $conditions) {
                    $cond = new CurrentConditions();
                    $cityID = $conditions->id;
                    $cond->city_id = $cityID;

                    /** @var CurrentConditions $last */
                    $last = $this->getLastUpdatedCurrentConditions($cityID);
                    /** @var Carbon $dateOfLastUpdate */
                    $dateOfLastUpdate = ($last ? $last->openweathermap_update_time : null);
                    $updateTime = DateTime::createFromFormat('U', $conditions->dt, timezone_open("UTC"));
                    if ($dateOfLastUpdate && $dateOfLastUpdate->eq($updateTime)) {
                        $last->update_time = Carbon::now("UTC");
                        $last->save();
                        continue;
                    }
                    $cond->openweathermap_update_time = $updateTime;
                    $cond->update_time = Carbon::now("UTC");

                    $cond->temperature = $conditions->main->temp;
                    $cond->feels_like_temperature = $conditions->main->feels_like;
                    $cond->pressure = $conditions->main->pressure;
                    $cond->humidity = $conditions->main->humidity / 100.0;
                    $cond->cloudiness = $conditions->clouds->all / 100.0;

                    $wind = $conditions->wind;
                    $cond->wind_speed = $wind->speed;
                    $cond->wind_direction = (property_exists($wind, "deg") ? $wind->deg : null);
                    $cond->wind_gust = (property_exists($wind, "gust") ? $wind->gust : null);

                    if (property_exists($conditions, "rain")) {
                        $rain = $conditions->rain;
                        $cond->rain_1h = (property_exists($rain, "1h") ? $rain->{"1h"} : null);
                        $cond->rain_3h = (property_exists($rain, "3h") ? $rain->{"3h"} : null);
                    }
                    if (property_exists($conditions, "snow")) {
                        $snow = $conditions->snow;
                        $cond->snow_1h = (property_exists($snow, "1h") ? $snow->{"1h"} : null);
                        $cond->snow_3h = (property_exists($snow, "3h") ? $snow->{"3h"} : null);
                    }

                    $weathers = $conditions->weather;
                    DB::transaction(function () use ($cond, $weathers, &$condIDs) {
                        $cond->save();
                        $condID = $cond->current_conditions_id;
                        $condIDs[] = $condID;

                        foreach ($weathers as $weather) {
                            DB::table("current_conditions_weather")->insert([
                                "current_conditions_id" => $condID,
                                "openweathermap_weather_id" => $weather->id
                            ]);
                        }
                    }, 5);
                }
            }

            if (!($data && $expectedChunkSize === count($data))) {
                // getting data for less cities than expected (some cityID is wrong or limit of requests was exceeded)
                // TODO
            }
        }

        $res = CurrentConditions::whereIn("current_conditions_id", $condIDs)->with(["weathers", "city"])->get()->all();
        return $res;
    }

    private function getLastUpdatedCurrentConditions(int $cityID) {
        return CurrentConditions::where("city_id", $cityID)->orderBy("update_time", "desc")->limit(1)->first();
    }


}