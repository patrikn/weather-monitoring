<?php

namespace App\Console\Commands;

use App\Model\City;
use DateTime;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Date;

class SaveSunsetAndSunriseForCities extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:save-sunset-sunrise';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = ''; // TODO

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    private function getSunriseAndSunset(DateTime $date, float $latitude, float $longitude)
    {
        // Calculate current Julian day
        $year = $date->format("Y") * 1;
        $month = $date->format("n") * 1;
        $day = $date->format("j") * 1;
        $jdn = (1461 * ($year + 4800 + ($month - 14)/12))/4
            + (367 * ($month - 2 - 12 * (($month - 14)/12)))/12
            - (3 * (($year + 4900 + ($month - 14)/12)/100))/4
            + $day - 32075;
        $n = $jdn - 2451545.0 + 0.0008;

        // Mean solar noon
        $lw = $longitude /*($longitude < 0 ? $longitude * -1 : $longitude)*/;
        $js = $n - $lw / 360;

        // Solar mean anomaly
        $m = round((357.5291 + 0.98560028 * $js)) % 360;

        // Equation of the center
        $c = 1.9148 * sin(deg2rad($m)) + 0.0200 * sin(deg2rad(2*$m)) + 0.003 * sin(deg2rad(3*$m));

        // Ecliptic longitude
        $lambda = round(($m + $c + 180 + 102.9372)) % 360;

        // Solar transit
        $jt = 2451545.0 + $js + 0.0053 * sin(deg2rad($m)) + 0.0069 * sin(deg2rad(2 * $lambda));

        // Declination of the Sun
        $delta = rad2deg(asin(sin(deg2rad($lambda)) * sin(deg2rad(23.44))));

        // Hour angle
        $ln = $latitude;// north latitude
        $omega = rad2deg(acos(
            (sin(deg2rad(-0.83)) - sin(deg2rad($ln)) * sin(deg2rad($lambda)))
            / (cos(deg2rad($ln)) * cos(deg2rad($lambda)))
        ));

        echo "top = " .  (sin(deg2rad(-0.83)) - sin(deg2rad($ln)) * sin(deg2rad($lambda))) . "\n";
        echo "bot = " .  (cos(deg2rad($ln)) * cos(deg2rad($lambda))) . "\n";
        echo "". (sin(deg2rad(-0.83)) - sin(deg2rad($ln)) * sin(deg2rad($lambda)))
            / (cos(deg2rad($ln)) * cos(deg2rad($lambda))) . "\n";

        // Sunrise and sunset
        $sunrise = $jt - $omega / 360;
        $sunset = $jt + $omega / 360;

        $result = [
            "sunrise" => $sunrise,
            "sunset" => $sunset
        ];
        var_dump($result);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // TODO
        //$cities = City::whereIn("openweathermap_city_id", [/*3451190,*/ 3067696/*, 2147714*/])->get();
        /*$now = new DateTime();
        foreach ($cities as $city) {
            echo $this->getSunriseAndSunset($now, $city->lat, $city->lon) . "\n";
        }*/
    }
}
