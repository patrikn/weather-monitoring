@extends('base.base')

@section('title', 'Správa miest')

@section("styles")
    <link type="text/css" rel="stylesheet" href="{{asset('css/datatables.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('css/select2.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('css/icheck.css')}}">
@endsection

@section("content")
    <div class="row">
        <section class="col-12 col-md-4">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        Pridať nové mesto
                    </h3>
                </div>
                <div class="card-body">
                    <form role="form" id="addCityForm">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="cityName">Názov</label>
                                    <input id="cityName" type="text" class="form-control form-control-sm">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="addCityContinentSelect">Kontinent</label>
                                    <select id="addCityContinentSelect"
                                            class="form-control continent-select select2 select2bs4 select2-hidden-accessible"
                                            name="continent"
                                            data-dropdown-css-class="select2-danger"
                                            style="width: 100%;"
                                            aria-hidden="true">
                                        <option value="all" selected="selected">Všetky kontinenty</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="addCityCountrySelect">Krajina</label>
                                    <select id="addCityCountrySelect"
                                            class="form-control country-select select2 select2bs4 select2-hidden-accessible"
                                            name="country"
                                            data-dropdown-css-class="select2-danger"
                                            style="width: 100%;"
                                            aria-hidden="true">
                                        <option value="all" selected="selected">Všetky krajiny</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="cityLat">Zemepisná šírka</label>
                                    <input id="cityLat" type="text" class="form-control form-control-sm">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="cityLon">Zemepisná dĺžka</label>
                                    <input id="cityLon" type="text" class="form-control form-control-sm">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group clearfix">
                                    <div class="d-inline icheck-red">
                                        <input type="checkbox" id="checkboxPrimary1" checked="" class="">
                                        <label for="checkboxPrimary1" class="font-weight-normal ">
                                            Hlavné mesto
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 text-center">
                                <div class="form-group clearfix">
                                    <button id="addCitySubmitButton" type="submit" class="btn bg-red btn-flat btn-sm">Pridaj mesto</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <section class="col-12 col-md-8">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        Mestá
                    </h3>
                </div>
                <div class="card-body">
                    <div class="col-12" id="countriesFilterForm">
                        <div class="row" id="continentsFilter">
                            <div class="col-12 col-sm-6 col-md-4 form-group">
                                <label>Kontinent</label>
                                <select class="form-control continent-select select2 select2bs4 select2-hidden-accessible"
                                        name="continent"
                                        data-dropdown-css-class="select2-danger"
                                        style="width: 100%;"
                                        aria-hidden="true">
                                    <option value="all" selected="selected">Všetky kontinenty</option>
                                </select>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 form-group">
                                <label>Krajina</label>
                                <select class="form-control country-select select2 select2bs4 select2-hidden-accessible"
                                        name="country"
                                        data-dropdown-css-class="select2-danger"
                                        style="width: 100%;"
                                        aria-hidden="true">
                                    <option value="all" selected="selected">Všetky krajiny</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="cities" class="table table-bordered table-hover dataTable table-sm"
                                       role="grid">
                                    <thead>
                                    <tr>
                                        <th class="hidden">Kód kontinentu</th>
                                        <th>Kontinent</th>
                                        <th class="hidden">Kód krajiny</th>
                                        <th>Krajina</th>
                                        <th>Mesto</th>
                                        <th>Počet obyvateľov</th>
                                        <th>Zemepisná šírka</th>
                                        <th>Zemepisná dĺžka</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </section>
    </div>
@endsection

@section("scripts")
    <script type="text/javascript" src="{{asset('js/datatables.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/select2.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/admin-cities.js')}}"></script>
@endsection