@extends('base.base')

@section('title', 'Homepage')

@section('content')
    <div class="container-fluid">

        <!-- Main row -->
        <div class="row text-center">
            <!-- Left col -->
            <section class="col-6">
                @foreach($countries[1]->cities as $city)
                    <a class="btn btn-flat btn-xs bg-cyan"
                       href="/city/{{$city->openweathermap_city_id}}"
                       style="margin-bottom: 2px"
                    >
                        {{$city->name}}
                    </a>
                @endforeach
            </section>
            <!-- /.Left col -->
            <!-- Left col -->
            <section class="col-6">
                @foreach($countries[0]->cities as $city)
                    <a class="btn btn-flat btn-xs bg-cyan"
                       href="/city/{{$city->openweathermap_city_id}}"
                       style="margin-bottom: 2px"
                    >
                        {{$city->name}}
                    </a>
                @endforeach
            </section>
            <!-- /.Left col -->

        </div>
        <!-- /.row (main row) -->

    </div><!-- /.container-fluid -->
@endsection