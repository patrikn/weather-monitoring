<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>
        Počasie
        @hasSection('title')
            | @yield('title')
        @endif
    </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <link type="text/css" rel="stylesheet" href="{{asset('css/weather-icons.css')}}">

    <link type="text/css" rel="stylesheet" href="{{asset('css/adminlte-plugins.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('css/adminlte.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('css/app.css')}}">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    @yield('styles')
</head>
<body class="hold-transition sidebar-mini layout-fixed accent-cyan text-sm">
<div class="wrapper">

    @yield('navbar', View::make('base.navbar'))

    @yield('sidebar', View::make('base.sidebar'))

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @yield('page-header', View::make('base.page-header'))
        <!-- Main content -->
            <section class="content">
                @yield('content')
            </section>
            <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    @yield('footer', View::make('base.footer'))

</div>
<!-- ./wrapper -->

<script src="{{asset('js/adminlte-plugins.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<script src="{{asset('js/adminlte.js')}}"></script>

@yield('scripts')

</body>

</html>
