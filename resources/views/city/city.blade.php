@extends('base.base')

@section('title', $city->name)

@section('styles')

@endsection

@section('content')
    <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <div class="col-lg-4 col-xl-3 col-md-5 col-sm-6 col-xs-12 connectedSortable">
                <div class="card bg-gradient-cyan" id="currentWeather">
                    @if($currentCondition !== null)
                    <div class="card-body text-center">
                        <div class="row">
                            <div class="col-12 current-temperature">
                                <span class="temperature">{{number_format($currentCondition->temperature, 0)}}</span>
                                {{--<span class="decimals">.{{number_format(round($currentCondition->temperature / 10.0))}}</span>--}}
                                <span class="unit">°C</span>
                            </div>
                        </div>
                        <div class="row current-conditions text-left">
                            <div class="col-12 current-weather">
                                <div class="icon float-left">
                                    <i class="wi wi-owm-{{$currentCondition->weathers[0]->openweathermap_weather_id}}"></i>
                                </div>
                                @php
                                    $desc = implode(", ", array_map(function($item) {
                                        return $item->description;
                                    }, $currentCondition->weathers->all()));
                                @endphp
                                <div class="text">{{$desc}}</div>
                            </div>
                            <div class="col-12 current-weather">
                                <div class="icon float-left" aa="{{(($currentCondition->wind_direction ?: 0) % 360) / 360.0}}">
                                    <i class="wi wi-wind towards-{{($currentCondition->wind_direction ?: 0) % 360}}-deg"></i>
                                </div>
                                <div class="text">{{$currentCondition->wind_speed}} m/s</div>
                            </div>
                            <div class="col-12 current-weather">
                                <div class="icon float-left"><i class="wi wi-humidity"></i></div>
                                <div class="text">{{number_format($currentCondition->humidity * 100, 0)}}%</div>
                            </div>
                            <div class="col-12 current-weather">
                                <div class="icon float-left"><i class="wi wi-barometer"></i></div>
                                <div class="text">{{number_format($currentCondition->pressure, 0, ",", "")}} hPa</div>
                            </div>
                            <div class="col-12 current-weather">
                                <div class="icon float-left"><i class="fas fa-cloud"></i></div>
                                <div class="text">{{number_format($currentCondition->cloudiness * 100, 0)}}%</div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer last-updated">
                        <i class="far fa-clock icon"></i>
                        {{$currentCondition->openweathermap_update_time->setTimezone(new DateTimeZone("Europe/Bratislava"))->format('j.n.Y G:i')}}
                    </div>
                    @else
                        <div class="card-body text-center">
                            <div class="row">
                                <div class="col-12">
                                    Žiadne dáta
                                </div>
                            </div>
                        </div>
                @endif
                    <!-- /.card-footer -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.Left col -->

            <!-- right col (We are only adding the ID to make the widgets sortable)-->
            <div class="col-lg-8 col-xl-9 col-md-7 col-sm-6 col-xs-12 connectedSortable">
                <div class="card" id="weatherForecast">
                    <div class="card-header bg-cyan">
                        <h3 class="card-title">Predpoveď počasia</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body text-center">
                        <div class="row">
                            @php
                                $counter = 0;
                            @endphp
                            @foreach ($weatherForecast as $dayIndex => $forecast)
                                @php($counter++)
                                @if($counter > 2)
                                    <div class="col-md-4">
                                        <div class="card bg-gradient-cyan">
                                            <div class="card-body text-center">
                                                <div class="row dayname">
                                                    <div class="col-12 text-center text-uppercase text-sm">
                                                        {{$dayIndex}}
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 text-center text-uppercase text-lg">
                                                        <i class="wi wi-owm-{{$forecast["averageWeather"]->openweathermap_weather_id}}"></i>
                                                    </div>
                                                    <div class="col-12 text-center text-uppercase text-sm">
                                                        {{$forecast["averageWeather"]->description}}
                                                    </div>
                                                </div>
                                                <div class="row temperature smaller">
                                                    <div class="col-lg-12 col-6 col-md-12 col-xl-6 col-sm-6 text-center">
                                                        <span class="icon"><i class="fas fa-arrow-circle-down"></i></span>
                                                        <span class="value">{{$forecast["minTemperature"]}}</span>
                                                        <span class="unit">°C</span>
                                                    </div>
                                                    <div class="col-lg-12 col-6 col-md-12 col-xl-6 col-sm-6 text-center">
                                                        <span class="icon"><i class="fas fa-arrow-circle-up"></i></span>
                                                        <span class="value">{{$forecast["maxTemperature"]}}</span>
                                                        <span class="unit">°C</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @else

                                    <div class="col-md-6">
                                        <div class="card bg-gradient-cyan">
                                            <div class="card-body text-center">
                                                <div class="row dayname">
                                                    <div class="col-12 text-center text-uppercase text-md">
                                                        {{$dayIndex}}
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 text-center text-uppercase text-xl">
                                                        <i class="wi wi-owm-{{$forecast["averageWeather"]->openweathermap_weather_id}}"></i>
                                                    </div>
                                                    <div class="col-12 text-center text-uppercase text-sm">
                                                        {{$forecast["averageWeather"]->description}}
                                                    </div>
                                                </div>
                                                <div class="row temperature">
                                                    <div class="col-lg-6 col-6 col-md-12 col-xl-6 col-sm-12 text-center">
                                                        <span class="icon"><i class="fas fa-arrow-circle-down"></i></span>
                                                        <span class="value">{{$forecast["minTemperature"]}}</span>
                                                        <span class="unit">°C</span>
                                                    </div>
                                                    <div class="col-lg-6 col-6 col-md-12 col-xl-6 col-sm-12 text-center">
                                                        <span class="icon"><i class="fas fa-arrow-circle-up"></i></span>
                                                        <span class="value">{{$forecast["maxTemperature"]}}</span>
                                                        <span class="unit">°C</span>
                                                    </div>
                                                </div>
                                                <div class="row wind-pressure-rain-snow">
                                                    <div class="col-lg-4 col-4 col-md-12 col-sm-12 col-xs-12 text-center">
                                                        <span class="icon"><i class="wi wi-wind"></i></span>
                                                        <span class="value">{{$forecast["maxWindSpeed"]}}</span>
                                                        <span class="unit">m/s</span>
                                                    </div>
                                                    <div class="col-lg-4 col-4 col-md-12 col-sm-12 col-xs-12 text-center">
                                                        <span class="icon"><i class="wi wi-barometer"></i></span>
                                                        <span class="value">{{$forecast["averagePressure"]}}</span>
                                                        <span class="unit">hPa</span>
                                                    </div>
                                                    <div class="col-lg-4 col-4 col-md-12 col-sm-12 col-xs-12 text-center">
                                                        @if($forecast["rain"] != 0)
                                                            <span class="icon"><i class="wi wi-raindrop"></i></span>
                                                            <span class="value">{{$forecast["rain"]}}</span>
                                                        @elseif($forecast["snow"] != 0)
                                                            <span class="icon"><i class="wi wi-snowflake-cold"></i></span>
                                                            <span class="value">{{$forecast["snow"]}}</span>
                                                        @else
                                                            <span class="icon"><i class="wi wi-raindrop"></i></span>
                                                            <span class="value">0</span>
                                                        @endif
                                                        <span class="unit">mm</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                @endif

                             @endforeach
                        </div>
                    </div>
                </div>
                <!-- /.card -->
            </div>
            <!-- right col -->

            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-cyan">
                        <h3 class="card-title">
                            <i class="fas fa-clock"></i>
                            História
                        </h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body text-center">
                        <div class="chart">
                            <canvas id="myChart"></canvas>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
@endsection

@section('scripts')
    <script type="text/javascript" src="{{asset('js/chartjs.js')}}"></script>
    <script>
        let jsonData = @json($conditionsHistory);
        const dataTemperature = jsonData.map(function(condition) {
            return {
                x: moment.unix(condition.date),
                y: Math.round(condition.temperature * 10) / 10
            }
        });
        const dataFeelsLike = jsonData.map(function(condition) {
            return {
                x: moment.unix(condition.date),
                y: Math.round(condition.feels_like_temp * 10) / 10
            }
        });
        const dataWindSpeed = jsonData.map(function(condition) {
            return {
                x: moment.unix(condition.date),
                y: Math.round(condition.wind_speed * 10) / 10
            }
        });

        var config = {
            type: 'line',
            data: {
                datasets: [{
                    yAxisID: 'tempAxis',
                    pointBackgroundColor: "#F00",
                    pointRadius: 4,
                    borderColor: "#F00",
                    borderWidth: 2,
                    label: 'Teplota vzduchu',
                    fill: false,
                    data: dataTemperature
                }, /*{
                    yAxisID: 'tempAxis',
                    pointRadius: 0,
                    borderColor: "#ff4f84",
                    borderWidth: 2,
                    label: 'Pocitová teplota',
                    fill: false,
                    data: dataFeelsLike
                },*/ {
                    yAxisID: 'speedAxis',
                    pointBackgroundColor: "#00F",
                    pointRadius: 0,
                    borderColor: "#00F",
                    borderWidth: 2,
                    label: 'Rýchlosť vetra',
                    fill: true,
                    data: dataWindSpeed
                }]
            },
            options: {
                responsive: true,
                title: {
                    display: false,
                    text: 'Chart.js Time Point Data'
                },
                legend: {
                    display: true,
                    position: "bottom"
                },
                scales: {
                    xAxes: [{
                        type: 'time',
                        display: true,
                        /*scaleLabel: {
                            display: true,
                            labelString: 'Date'
                        },*/
                        ticks: {
                            major: {
                                fontStyle: 'bold',
                                fontColor: '#FF0000'
                            },
                        },
                        time: {
                            displayFormats: {
                                hour: 'H:mm'
                            }
                        }
                    }],
                    yAxes: [{
                        id: "tempAxis",
                        display: true,
                        position: 'left',
                        ticks : {
                            beginAtZero : true
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'teplota [°C]'
                        }
                    }, {
                        id: "speedAxis",
                        display: true,
                        position: 'right',
                        ticks : {
                            beginAtZero : true
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'rýchlosť [m/s]'
                        }
                    }]
                }
            }
        };

        window.onload = function() {
            var ctx = document.getElementById('myChart').getContext('2d');
            window.myLine = new Chart(ctx, config);
        };

        /*let lineChart = new Chart(ctx, {
            type: 'line',
            data: data,
            options: lineChartOptions
        })*/

        /*var donutChartCanvas = $('#myChart').get(0).getContext('2d');
        var donutData        = {
            labels: [
                'Chrome',
                'IE',
                'FireFox',
                'Safari',
                'Opera',
                'Navigator',
            ],
            datasets: [
                {
                    data: [700,500,400,600,300,100],
                    backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
                }
            ]
        }
        var donutOptions     = {
            maintainAspectRatio : false,
            responsive : true,
        }
        //Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
        var donutChart = new Chart(donutChartCanvas, {
            type: 'doughnut',
            data: donutData,
            options: donutOptions
        })*/
    </script>
@endsection