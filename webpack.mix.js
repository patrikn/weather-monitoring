const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');

/* Theme style */
mix.styles([
    'vendor/almasaeed2010/adminlte/dist/css/adminlte.min.css'
], 'public/css/adminlte.css');

mix.copyDirectory('vendor/almasaeed2010/adminlte/plugins/fontawesome-free/webfonts', 'public/webfonts');

mix.styles([
    'vendor/almasaeed2010/adminlte/plugins/fontawesome-free/css/all.min.css', /* Font Awesome */
    'vendor/almasaeed2010/adminlte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css', /* overlayScrollbars */
], 'public/css/adminlte-plugins.css');

mix.scripts([
    'vendor/almasaeed2010/adminlte/dist/js/adminlte.js', /* AdminLTE App */
], 'public/js/adminlte.js');

mix.scripts([
    'vendor/almasaeed2010/adminlte/plugins/jquery/jquery.min.js', /* jQuery */
    'vendor/almasaeed2010/adminlte/plugins/jquery-ui/jquery-ui.min.js', /* jQuery UI 1.11.4 4 */
    'vendor/almasaeed2010/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js', /* Bootstrap 4 */
    'vendor/almasaeed2010/adminlte/plugins/moment/moment.min.js',
    'vendor/almasaeed2010/adminlte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js', /* overlayScrollbars */
], 'public/js/adminlte-plugins.js');

mix.copy('vendor/almasaeed2010/adminlte/dist/img/AdminLTELogo.png', 'public/img/AdminLTELogo.png');

mix.scripts(['resources/js/admin-cities.js'], 'public/js/admin-cities.js');

/* datatables */
mix.scripts([
    'vendor/almasaeed2010/adminlte/plugins/datatables/jquery.dataTables.js',
    'vendor/almasaeed2010/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js'
], 'public/js/datatables.js');
mix.styles([
    'vendor/almasaeed2010/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css',
], 'public/css/datatables.css');

/* select2 */
mix.styles([
    'vendor/almasaeed2010/adminlte/plugins/select2/css/select2.min.css',
    'vendor/almasaeed2010/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css',
], 'public/css/select2.css');
mix.scripts([
    'vendor/almasaeed2010/adminlte/plugins/select2/js/select2.full.min.js',
], 'public/js/select2.js');

/* iCheck */
mix.styles([
    'vendor/almasaeed2010/adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css',
], 'public/css/icheck.css');

/* weather-icons */
mix.styles([
    'lib/weather-icons-master/css/weather-icons.css',
    'lib/weather-icons-master/css/weather-icons-wind.css',
], 'public/css/weather-icons.css');
mix.copyDirectory('lib/weather-icons-master/font', 'public/font');

/* chartJs */
mix.scripts([
    'vendor/almasaeed2010/adminlte/plugins/chart.js/Chart.min.js',
], 'public/js/chartjs.js');
