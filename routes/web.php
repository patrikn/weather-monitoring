<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "HomepageController@render")->name("homepage");

Route::get('/admin', "AdminController@render")->name("admin");
Route::get('/admin/cities', "AdminController@renderCities")->name("admin.cities");

Route::get("/city/{cityID}", "CityController@renderCity")->name("city.get");
