<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get("/extract", "ExternalApiController@extract");

Route::get("/cities", "InternalApiController@getCities")->name("cities.get");

Route::get("/continents/{continentCode}", "InternalApiController@getCountriesByContinentCode")->name("countriesByContinent.get");

Route::get("/continents", "InternalApiController@getContinents")->name("continents.get");
