$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        var selectedContinentCode = $('#countriesFilterForm .continent-select').val();
        var selectedCountryCode = $('#countriesFilterForm .country-select').val();
        var continentCode = data[0];
        var countryCode = data[2];

        if (
            (selectedCountryCode !== "all" && selectedCountryCode === countryCode) ||
            (selectedCountryCode === "all" && selectedContinentCode !== null && selectedContinentCode === continentCode) ||
            (selectedContinentCode === "all" && selectedCountryCode === "all")
        ) {
            return true;
        }
        return false;
    }
);

function renderCountrySelect(selector, continentCode) {
    $(selector).val("all").trigger("change");
    $(selector).select2({
        ajax: {
            url: `/api/continents/${continentCode}`,
            processResults: function (json) {
                let data = [];
                if (continentCode === "all") {
                    json.forEach(function(continent) {
                        continent.countries.forEach(function(country) {
                            data.push({id: country.code, text: country.name});
                        });
                    });
                } else {
                    json.countries.forEach(function(item) {
                        data.push({id: item.code, text: item.name});
                    });
                }
                data.sort(function(country1, country2) {
                    return country1.text.localeCompare(country2.text);
                });
                data.unshift({id: "all", text: "Všetky krajiny", selected: true});
                return {
                    results: data
                };
            }
        },
        minimumResultsForSearch: -1
    });
}

$(document).ready(function() {

    let table = $('#cities').DataTable({
        ajax: {
            url: "/api/cities",
            dataSrc: function(json) {
                data = [];
                json.forEach(function(city) {
                    city.country.continents.forEach(function (continent) {
                        data.push({
                            continentCode: continent.code,
                            continent: continent.name,
                            countryCode: city.country.code,
                            country: city.country.name,
                            city: city.name,
                            population: city.population,
                            lat: city.lat,
                            lon: city.lon
                        });
                    });
                });
                return data;
            }
        },
        columns: [
            {data: "continentCode", visible: false},
            {data: "continent", visible: true},
            {data: "countryCode", visible: false},
            {data: "country",visible: true},
            {data: "city",visible: true},
            {data: "population",visible: true},
            {data: "lat",visible: true},
            {data: "lon",visible: true}
        ]
    });

    $('.continent-select').select2({
        ajax: {
            url: "/api/continents",
            processResults: function (json) {
                let data = [{id: "all", text: "Všetky kontinenty", selected: true}];
                json.forEach(continent => {
                    data.push({id: continent.code, text: continent.name})
                });
                return {
                    results: data
                };
            },
        },
        minimumResultsForSearch: -1
    });

    renderCountrySelect('#countriesFilterForm .country-select', $('#countriesFilterForm .continent-select').val());
    $('#countriesFilterForm .continent-select').on('change', function (e) {
        const selectedContinentCode = $(this).val();
        renderCountrySelect('#countriesFilterForm .country-select', selectedContinentCode);
        table.draw();
    });
    $('#countriesFilterForm .country-select').on('change', function (e) {
        table.draw();
    });

    renderCountrySelect('#addCityForm .country-select', $('#addCityForm .continent-select').val());
    $('#addCityForm .continent-select').on('change', function (e) {
        const selectedContinentCode = $(this).val();
        renderCountrySelect('#addCityForm .country-select', selectedContinentCode);
    });
});